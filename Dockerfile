FROM python:3.10-alpine

LABEL maintainer="Ilya Ghost"

ENV BOT_TOKEN=

WORKDIR /app

COPY requirements.txt /app

RUN pip install -r requirements.txt

COPY . .

RUN mkdir History

CMD ["python", "./main.py"]
